import { Component, inject } from '@angular/core';
import { ArticleComponent } from '../article/article.component';
import { CategoriesComponent } from '../categories/categories.component';
import { LatestPostsComponent } from '../latest-posts/latest-posts.component';
import { TagsComponent } from '../tags/tags.component';
import { CommentFormComponent } from '../comment-form/comment-form.component';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-about-page',
  standalone: true,
  imports: [
    ArticleComponent,
    CategoriesComponent,
    LatestPostsComponent,
    TagsComponent,
    CommentFormComponent,
  ],
  templateUrl: './about-page.component.html',
  styleUrl: './about-page.component.css',
})
export class AboutPageComponent {
  article = {
    title: 'The Future of Farming, Smart Irrigation Solutions',
    body: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corrupti, aspernatur molestiae nemo totam neque harum doloribus odit voluptates architecto repudiandae ex assumenda. Sint ea dicta illum accusamus voluptatum. Voluptates pariatur repellendus, ea inventore culpa placeat eveniet vitae aliquam amet quas! Incidunt cupiditate voluptatum consequatur repudiandae perspiciatis nobis tempore ullam iusto at, consectetur illum praesentium, excepturi voluptate ducimus tenetur libero expedita quaerat! Atque quia officiis, est temporibus consequuntur impedit odio facere qui rem unde aspernatur vero ab veritatis nisi doloribus hic, maiores itaque. Nam autem distinctio optio est nemo maxime sed porro eum? Sequi vitae deleniti odit nostrum similique, nisi laboriosam accusantium recusandae, voluptas corrupti debitis assumenda quis blanditiis error? Ad veritatis dicta quidem. Officiis similique tempore necessitatibus atque fuga at culpa iure fugiat numquam magnam repudiandae mollitia natus eveniet illum quaerat ut, fugit, saepe minima vero aliquam a minus maiores? Qui dignissimos nemo dolore dolores. Molestiae error commodi iure asperiores porro placeat ipsa modi sed. Rem magni deserunt pariatur maxime sunt? Earum laboriosam architecto quia quod natus, recusandae incidunt facere commodi non id quo. Nulla culpa rem ab eius officiis dolorem veniam quia vero possimus, repellat neque, nisi dolore minima quo labore expedita quibusdam ducimus aspernatur excepturi id eum, dolorum consequatur. Repellat est reiciendis exercitationem praesentium veniam dolor neque, odio mollitia similique officia natus ipsam?',
  };

  postsService: PostsService = inject(PostsService);

  categories = ['Engineering', 'Farm', 'Farming'];
  tags = ['Agriculture', 'Organic', 'Vegetables'];
}
