import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  latestPosts = [
    { title: 'Bringing Food Production Back To Cities' },
    { title: 'Agronomy and relation to Other Sciences' },
    { title: 'The Future of Farming, Smart Irrigation Solutions' },
  ];

  constructor() {}
}
