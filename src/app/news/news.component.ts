import { Component } from '@angular/core';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-news',
  standalone: true,
  imports: [NgFor],
  templateUrl: './news.component.html',
  styleUrl: './news.component.css',
})
export class NewsComponent {
  articles = [
    {
      date: '05 July 2022',
      title: 'Bringing Food Production Back To Cities',
      description: 'Description of the article goes here.',
      author: 'Evelyn Martin',
      comments: 0,
      image: 'assets/article-1.png',
    },
    {
      date: '05 July 2022',
      title: 'The Future of Farming, Smart Irrigation Solutions',
      description: 'Description of the article goes here.',
      author: 'Evelyn Martin',
      comments: 0,
      image: 'assets/article-2.png',
    },
    {
      date: '05 July 2022',
      title: 'Agronomy and relation to Other Sciences',
      description: 'Description of the article goes here.',
      author: 'Evelyn Martin',
      comments: 0,
      image: 'assets/article-3.png',
    },
    {
      date: '05 July 2022',
      title: 'We grow products with the organic farming',
      description: 'Description of the article goes here.',
      author: 'Evelyn Martin',
      comments: 0,
      image: 'assets/article-1.png',
    },
    {
      date: '05 July 2022',
      title: 'A Quick Solution to Low Milk Production in Zimbabwe',
      description: 'Description of the article goes here.',
      author: 'Evelyn Martin',
      comments: 0,
      image: 'assets/article-3.png',
    },
    {
      date: '05 July 2022',
      title: 'Winter wheat harvest organic gather nice moment',
      description: 'Description of the article goes here.',
      author: 'Evelyn Martin',
      comments: 0,
      image: 'assets/article-2.png',
    },
  ];
}
