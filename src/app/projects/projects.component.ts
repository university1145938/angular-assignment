import { Component } from '@angular/core';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-projects',
  standalone: true,
  imports: [NgFor],
  templateUrl: './projects.component.html',
  styleUrl: './projects.component.css',
})
export class ProjectsComponent {
  projects = [
    { title: 'Project 1', image: 'assets/similar-projects-1.png' },
    { title: 'Project 2', image: 'assets/similar-projects-2.png' },
    { title: 'Project 3', image: 'assets/similar-projects-3.png' },
    { title: 'Project 4', image: 'assets/similar-projects-2.png' },
  ];
}
