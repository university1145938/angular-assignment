import { Component, Input } from '@angular/core';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-latest-posts',
  standalone: true,
  imports: [NgFor],
  templateUrl: './latest-posts.component.html',
  styleUrl: './latest-posts.component.css',
})
export class LatestPostsComponent {
  @Input() latestPosts: any;
}
