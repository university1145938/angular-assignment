import { Component } from '@angular/core';
import { NgIf } from '@angular/common';

import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-comment-form',
  standalone: true,
  imports: [ReactiveFormsModule, NgIf],
  templateUrl: './comment-form.component.html',
  styleUrl: './comment-form.component.css',
})
export class CommentFormComponent {
  submit() {
    console.log('submitted');
    console.log(this.commentForm.value);
  }

  commentForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.commentForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      comment: ['', [Validators.required]],
    });
  }
}
